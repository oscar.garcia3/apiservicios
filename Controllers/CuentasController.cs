﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ServicioPublicosAPI.DTOs;
using ServicioPublicosAPI.Models.Auth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Controllers
{
    [Route("api/cuentas")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IConfiguration configuration;
        private readonly SignInManager<AppUser> signInManager;

        public CuentasController(UserManager<AppUser> userManager,IConfiguration configuration, SignInManager<AppUser> signInManager)
        {
            this.userManager = userManager;
            this.configuration = configuration;
            this.signInManager = signInManager;
        }
        [HttpPost("registrar")]
        public async Task<IActionResult> Registrar([FromBody] DatosUsuario credenciales)
        {
            var usuario = new AppUser
            {
                UserName = credenciales.Username,
                Email = credenciales.Email,
                Name=credenciales.Name,
                SurName=credenciales.SurName
                
            };
            var result = await userManager.CreateAsync(usuario, credenciales.Password);
            if (result.Succeeded)
            {
                return Ok("Usuario Creado");
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<RespuestaAutenticacion>> Autenticar([FromBody] CredencialesAutenticacion credenciales)
        {
         
            var result = await signInManager.PasswordSignInAsync(credenciales.UserName, credenciales.Password,isPersistent:false,lockoutOnFailure:false);

            if (result.Succeeded)
            {
                return await ConstruirToken(credenciales);
            }
            else
            {
                return BadRequest("Login Incorrecto");
            }
        }


        private async Task<RespuestaAutenticacion> ConstruirToken(CredencialesAutenticacion credenciales)
        {
            
            var usuario = await userManager.FindByNameAsync(credenciales.UserName);
            var claims = new List<Claim>()
            {
                new Claim("Email", usuario.Email),
                new Claim("Nombre", usuario.Name),
                new Claim("Apellido", usuario.SurName)
            };
            var claimsDB = await userManager.GetClaimsAsync(usuario);
            claims.AddRange(claimsDB);

            var llave = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["jwtKey"]));
            var creds = new SigningCredentials(llave, SecurityAlgorithms.HmacSha256);
            var expiracion = DateTime.UtcNow.AddDays(1);
            var token = new JwtSecurityToken(issuer: null, audience: null, claims: claims, expires: expiracion, signingCredentials: creds);

            return new RespuestaAutenticacion()
            {
                Usuario= string.Concat(usuario.Name," ",usuario.SurName),
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiracion = expiracion
                
            };
        }

    }
}
