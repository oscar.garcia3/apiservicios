﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicioPublicosAPI.DTOs;
using ServicioPublicosAPI.Models;
using ServicioPublicosAPI.Models.Facturacion;
using ServicioPublicosAPI.Repos;
using ServicioPublicosAPI.Repos.Periodos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Controllers.Periodos
{
    [Route("api/periodos")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "EsAdmin")]
    public class PeriodosController : ControllerBase
    {
        private readonly IPeriodos periodos;
        private readonly applicationDbContext context;
        private readonly IMapper mapper;

        public PeriodosController(Repos.Periodos.IPeriodos periodos, applicationDbContext context,IMapper mapper)
        {
            this.periodos = periodos;
            this.context = context;
            this.mapper = mapper;
        }
        [HttpGet]
        public ActionResult<List<PeriodoDTO>> Get()
        {
            var lst = periodos.ListarTodos();
            return mapper.Map<List<PeriodoDTO>>(lst);
        }
       
        [HttpGet("{Id:int}")]
        public async Task<ActionResult<PeriodoFacturacion>> Get(int Id)
        {
            var periodo = await periodos.ObtenerPorId(Id);
            if(periodo==null)
            {
                return NotFound();
            }
            return periodo;
        }
        [HttpGet("activo")]
        public async Task<ActionResult<PeriodoFacturacion>> GetActivo()
        {
            var periodo = await periodos.ObtenerPeriodoActivo();
            if (periodo == null)
            {
                return NotFound();
            }
            return periodo;
        }

        [HttpPost]
        public ActionResult Post([FromBody] PeriodoFacturacion periodoFacturacion)
        {
            return NoContent();
        }
        [HttpPut]
        public ActionResult Put([FromBody] PeriodoFacturacion periodoFacturacion)
        {
            return NoContent();
        }
        [HttpDelete]
        public ActionResult Delete()
        {
            return NoContent();
        }
    }
}
