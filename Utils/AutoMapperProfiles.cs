﻿using AutoMapper;
using ServicioPublicosAPI.DTOs;
using ServicioPublicosAPI.Models.Facturacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Utils
{
    public class AutoMapperProfiles:Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<PeriodoFacturacion, PeriodoDTO>().ReverseMap();
        }
    }
}
