﻿using ServicioPublicosAPI.Models.Facturacion;
using ServicioPublicosAPI.Models.Medidores;
using ServicioPublicosAPI.Models.Rutas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models
{
    [Table("Suscriptores")]
    public class Suscriptor
    {
        [Column(Order = 0)]
        public int Id { get; set; }
        [Column(Order = 1)]
        [Required]
        public string Cedula { get; set; }

        [Column(Order = 2)]
        [Required]
        public string Nuid { get; set; }
        [Required]
        [MaxLength(100)]
        [Column(Order = 3)]
        public string PrimerNombre { get; set; }

        [MaxLength(100)]
        [Column(Order = 4)]
        public string SegundoNombre { get; set; }
        [Required]
        [MaxLength(100)]
        [Column(Order = 5)]
        public string PrimerApellido { get; set; }

        [MaxLength(100)]
        [Column(Order = 6)]
        public string SegundoApellido { get; set; }

        public int DireccionRutaID { get; set; }
        public DireccionesRuta Direccion { get; set; }
        [Column(Order = 9)]
        public string Email { get; set; }
        [Column(Order = 8)]
        public string Telefono { get; set; }
        [Column(Order = 9)]
        public string Extension { get; set; }
        [Required]
        public int UsoId { get; set; }
        public virtual Uso Uso { get; set; }
        [Required]
        public int EstratoId { get; set; }
        public virtual Estrato Estrato { get; set; }
        [Required]
        public int ZonaId { get; set; }
        public virtual Zona Zona { get; set; }
        public int MedidorId { get; set; }
        public virtual Medidor Medidor { get; set; }
        public bool Hogarcomunitario { get; set; }
        public string FichaCatastral { get; set; }
        public virtual ICollection<Consumo> Consumos { get; set; }
    }
}
