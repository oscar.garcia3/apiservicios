﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ServicioPublicosAPI.Models.Auth;
using ServicioPublicosAPI.Models.Facturacion;
using ServicioPublicosAPI.Models.Medidores;
using ServicioPublicosAPI.Models.Rutas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models
{
    public class applicationDbContext : IdentityDbContext<AppUser>
    {
        public applicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("ServiciosPublicos");
            modelBuilder.Entity<Consumo>().Property(o => o.ValorMedicion).HasPrecision(30, 10);
            modelBuilder.Entity<DescuentoAcueducto>().Property(o => o.Porcentaje).HasPrecision(5, 2);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<PeriodoFacturacion> PeriodosFacturacion { get; set; }
        public DbSet<Estrato> Estratos { get; set; }
        public DbSet<Uso> Usos { get; set; }
        public DbSet<Zona> Zonas { get; set; }
        public DbSet<Suscriptor> Suscriptores { get; set; }
        public DbSet<DescuentoAcueducto> DescuentoAcueducto { get; set; }
        public DbSet<Consumo> Consumos { get; set; }
        public DbSet<EstadoMedidor> Estados { get; set; }
        public DbSet<Medidor> Medidores { get; set; }
        public DbSet<TipoVia> TipoVias { get; set; }
        public DbSet<Ruta> Rutas { get; set; }
        public DbSet<RutaUsuarioPeriodo> RutaUsuarioPeriodo { get; set; }
        public DbSet<DireccionesRuta> DireccionesRuta { get; set; }


    }
}
