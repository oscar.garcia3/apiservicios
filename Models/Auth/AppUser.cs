﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Auth
{
    public class AppUser: IdentityUser
    {
        [MaxLength(200)]
        [Required]
        public string Name { get; set; }
        [MaxLength(200)]
        [Required]
        public string SurName { get; set; }
    }
}
