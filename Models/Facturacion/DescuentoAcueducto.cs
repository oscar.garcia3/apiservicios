﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Facturacion
{
    [Table("DescuentosAcueducto")]
    public class DescuentoAcueducto
    {
        public int Id { get; set; }

        public int EstratoId { get; set; }
        public Estrato Estrato { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
