﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Facturacion
{
    [Table("Periodos")]
    public class PeriodoFacturacion
    {
        [Key]
        public int PeriodoID { get; set; }
        [Required(ErrorMessage = "Campo Requerido")]
        public int Anio { get; set; }
        [Required(ErrorMessage = "Solo valores entre 1 y 12")]
        [Range(1, 12)]
        public int Mes { get; set; }
        [Required]
        public DateTime FechaInicial { get; set; }
        [Required]
        public DateTime FechaFinal { get; set; }
        [Required]
        public DateTime FechaCorte { get; set; }
        [Required]
        public DateTime FechaLimite { get; set; }
        public bool Abierto { get; set; }
        public bool Activo { get; set; }
        [Required]
        public int UsuarioID { get; set; }
        public string Observacion { get; set; }
        public bool Cancelado { get; set; }
        public virtual ICollection<Rutas.RutaUsuarioPeriodo> RutasPeriodo { get; set; }
    }
}
