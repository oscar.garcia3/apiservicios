﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Facturacion
{
    [Table("Consumos")]
    public class Consumo
    {
        public int Id { get; set; }
        [Required]
        public int PeriodoID { get; set; }
        public virtual PeriodoFacturacion Periodo { get; set; }
        public int SuscriptorID { get; set; }
        public virtual Suscriptor Suscriptor { get; set; }
        [Required]
        public decimal ValorMedicion { get; set; }
    }
}
