﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Medidores
{
    [Table("Medidores")]
    public class Medidor
    {
        public int Id { get; set; }
        public string IdMedidor { get; set; }
        public int EstadoId { get; set; }
        public virtual EstadoMedidor Estado { get; set; }
        public string Tipo { get; set; }
        public DateTime? FechaInstalacion { get; set; }
        public DateTime? FechaPrimeraLectura { get; set; }
        public virtual Suscriptor Suscriptor { get; set; }
    }
}
