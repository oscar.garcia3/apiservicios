﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Medidores
{
    [Table("EstadosMedidor")]
    public class EstadoMedidor
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Medidor> Medidores { get; set; }
    }
}
