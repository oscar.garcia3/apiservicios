﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models
{
    [Table("Zonas")]
    public class Zona
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(10)]
        public string Nombre { get; set; }
        public virtual ICollection<Suscriptor> Usuarios { get; set; }
    }
}
