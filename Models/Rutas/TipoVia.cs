﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Rutas
{
    [Table("TiposVia")]
    public class TipoVia
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}
