﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Rutas
{
    public class DireccionesRuta
    {
        public int Id { get; set; }
        public string TipoVia { get; set; }
        public string NumeroVia { get; set; }
        public string Prefijo { get; set; }
        public string NumeroViaGeneradora { get; set; }
        public string PrefijoViaGeneradora { get; set; }
        public string NumeroPlaca { get; set; }
        public int SuscriptorID { get; set; }
        public Suscriptor Suscriptor { get; set; }
        public string DireccionCompleta
        {
            get
            {
                return String.Concat(TipoVia, " ", NumeroVia, " ", Prefijo, " ", NumeroViaGeneradora, " ", PrefijoViaGeneradora, " ", NumeroPlaca).Replace("  ", " ");
            }
        }
        public string DireccionStandard { get; set; }
    }
}
