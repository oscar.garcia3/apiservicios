﻿using ServicioPublicosAPI.Models.Facturacion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models.Rutas
{
    [Table("RutasUsuarioPeriodo")]
    public class RutaUsuarioPeriodo
    {
        public int Id { get; set; }
        public int UsuarioId { get; set; }
        public int RutaId { get; set; }
        public Ruta Ruta { get; set; }
        public int PeriodoFacturacionId { get; set; }
        public DateTime FechaHoraInicio { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public string Observaciones { get; set; }
        public PeriodoFacturacion PeriodoFacturacion { get; set; }
    }
}
