﻿using ServicioPublicosAPI.Models.Facturacion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models
{
    [Table("Estratos")]
    public class Estrato
    {
        public int Id { get; set; }
        [Required]
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Suscriptor> Suscriptores { get; set; }
        public virtual ICollection<DescuentoAcueducto> DescuentosAcueducto { get; set; }
    }
}
