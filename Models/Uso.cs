﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Models
{
    [Table("Usos")]
    public class Uso
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Nombre { get; set; }
        public virtual ICollection<Suscriptor> Suscriptores { get; set; }
    }
}
