﻿using ServicioPublicosAPI.Models.Facturacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Repos.Periodos
{
    public interface IPeriodos
    {
        List<PeriodoFacturacion> ListarTodos();
        Task<PeriodoFacturacion> ObtenerPeriodoActivo();
        Task<PeriodoFacturacion> ObtenerPorId(int id);
    }
}
