﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.Repos.Periodos
{
    public class RepoPeriodos:IPeriodos
    {
        private List<Models.Facturacion.PeriodoFacturacion> _periodos;

        public RepoPeriodos()
        {
            _periodos = new List<Models.Facturacion.PeriodoFacturacion>()
            {
                new Models.Facturacion.PeriodoFacturacion()
                {
                    PeriodoID=1,
                    Anio=2021,
                    Mes=1,
                    FechaInicial=DateTime.Now,
                    FechaFinal=DateTime.Now,
                    FechaCorte=DateTime.Now,
                    FechaLimite=DateTime.Now,
                    UsuarioID=1,
                    Activo=true,
                    Abierto=true,
                },
                new Models.Facturacion.PeriodoFacturacion()
                {
                    PeriodoID=2,
                    Anio=2021,
                    Mes=2,
                    FechaInicial=DateTime.Now,
                    FechaFinal=DateTime.Now,
                    FechaCorte=DateTime.Now,
                    FechaLimite=DateTime.Now,
                    UsuarioID=1,
                    Activo=false,
                    Abierto=false,
                },
                new Models.Facturacion.PeriodoFacturacion()
                {
                    PeriodoID=3,
                    Anio=2021,
                    Mes=3,
                    FechaInicial=DateTime.Now,
                    FechaFinal=DateTime.Now,
                    FechaCorte=DateTime.Now,
                    FechaLimite=DateTime.Now,
                    UsuarioID=1,
                    Activo=false,
                    Abierto=false,
                },
                new Models.Facturacion.PeriodoFacturacion()
                {
                    PeriodoID=4,
                    Anio=2021,
                    Mes=4,
                    FechaInicial=DateTime.Now,
                    FechaFinal=DateTime.Now,
                    FechaCorte=DateTime.Now,
                    FechaLimite=DateTime.Now,
                    UsuarioID=1,
                    Activo=false,
                    Abierto=false,
                },

            };
        }
        public List<Models.Facturacion.PeriodoFacturacion> ListarTodos()
        {
            return _periodos;
        }
        public async Task<Models.Facturacion.PeriodoFacturacion> ObtenerPeriodoActivo()
        {
            await Task.Delay(TimeSpan.FromSeconds(3));
            return _periodos.FirstOrDefault(w => w.Activo);
        }
        public async Task<Models.Facturacion.PeriodoFacturacion> ObtenerPorId(int id)
        {
            await Task.Delay(TimeSpan.FromSeconds(3));
            return _periodos.FirstOrDefault(w => w.PeriodoID == id);
        }
    }
}
