﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioPublicosAPI.Migrations
{
    public partial class ModeloasMasIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EstadosMedidor",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadosMedidor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Estratos",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<int>(type: "int", nullable: false),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Estratos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rutas",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreRuta = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rutas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TiposVia",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TiposVia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usos",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Zonas",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zonas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Medidores",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdMedidor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EstadoId = table.Column<int>(type: "int", nullable: false),
                    Tipo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FechaInstalacion = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FechaPrimeraLectura = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medidores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Medidores_EstadosMedidor_EstadoId",
                        column: x => x.EstadoId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "EstadosMedidor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DescuentosAcueducto",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EstratoId = table.Column<int>(type: "int", nullable: false),
                    Porcentaje = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DescuentosAcueducto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DescuentosAcueducto_Estratos_EstratoId",
                        column: x => x.EstratoId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Estratos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RutasUsuarioPeriodo",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UsuarioId = table.Column<int>(type: "int", nullable: false),
                    RutaId = table.Column<int>(type: "int", nullable: false),
                    PeriodoFacturacionId = table.Column<int>(type: "int", nullable: false),
                    FechaHoraInicio = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FechaHoraFin = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Observaciones = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RutasUsuarioPeriodo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RutasUsuarioPeriodo_Periodos_PeriodoFacturacionId",
                        column: x => x.PeriodoFacturacionId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Periodos",
                        principalColumn: "PeriodoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RutasUsuarioPeriodo_Rutas_RutaId",
                        column: x => x.RutaId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Rutas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Suscriptores",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cedula = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nuid = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PrimerNombre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SegundoNombre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PrimerApellido = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SegundoApellido = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    DireccionRutaID = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefono = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Extension = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UsoId = table.Column<int>(type: "int", nullable: false),
                    EstratoId = table.Column<int>(type: "int", nullable: false),
                    ZonaId = table.Column<int>(type: "int", nullable: false),
                    MedidorId = table.Column<int>(type: "int", nullable: false),
                    Hogarcomunitario = table.Column<bool>(type: "bit", nullable: false),
                    FichaCatastral = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suscriptores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Suscriptores_Estratos_EstratoId",
                        column: x => x.EstratoId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Estratos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Suscriptores_Medidores_MedidorId",
                        column: x => x.MedidorId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Medidores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Suscriptores_Usos_UsoId",
                        column: x => x.UsoId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Usos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Suscriptores_Zonas_ZonaId",
                        column: x => x.ZonaId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Zonas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Consumos",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PeriodoID = table.Column<int>(type: "int", nullable: false),
                    SuscriptorID = table.Column<int>(type: "int", nullable: false),
                    ValorMedicion = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Consumos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Consumos_Periodos_PeriodoID",
                        column: x => x.PeriodoID,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Periodos",
                        principalColumn: "PeriodoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Consumos_Suscriptores_SuscriptorID",
                        column: x => x.SuscriptorID,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Suscriptores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DireccionesRuta",
                schema: "ServiciosPublicos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TipoVia = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumeroVia = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prefijo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumeroViaGeneradora = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PrefijoViaGeneradora = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumeroPlaca = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SuscriptorID = table.Column<int>(type: "int", nullable: false),
                    DireccionStandard = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RutaId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DireccionesRuta", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DireccionesRuta_Rutas_RutaId",
                        column: x => x.RutaId,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Rutas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DireccionesRuta_Suscriptores_SuscriptorID",
                        column: x => x.SuscriptorID,
                        principalSchema: "ServiciosPublicos",
                        principalTable: "Suscriptores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                schema: "ServiciosPublicos",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "ServiciosPublicos",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                schema: "ServiciosPublicos",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                schema: "ServiciosPublicos",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                schema: "ServiciosPublicos",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "ServiciosPublicos",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "ServiciosPublicos",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Consumos_PeriodoID",
                schema: "ServiciosPublicos",
                table: "Consumos",
                column: "PeriodoID");

            migrationBuilder.CreateIndex(
                name: "IX_Consumos_SuscriptorID",
                schema: "ServiciosPublicos",
                table: "Consumos",
                column: "SuscriptorID");

            migrationBuilder.CreateIndex(
                name: "IX_DescuentosAcueducto_EstratoId",
                schema: "ServiciosPublicos",
                table: "DescuentosAcueducto",
                column: "EstratoId");

            migrationBuilder.CreateIndex(
                name: "IX_DireccionesRuta_RutaId",
                schema: "ServiciosPublicos",
                table: "DireccionesRuta",
                column: "RutaId");

            migrationBuilder.CreateIndex(
                name: "IX_DireccionesRuta_SuscriptorID",
                schema: "ServiciosPublicos",
                table: "DireccionesRuta",
                column: "SuscriptorID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Medidores_EstadoId",
                schema: "ServiciosPublicos",
                table: "Medidores",
                column: "EstadoId");

            migrationBuilder.CreateIndex(
                name: "IX_RutasUsuarioPeriodo_PeriodoFacturacionId",
                schema: "ServiciosPublicos",
                table: "RutasUsuarioPeriodo",
                column: "PeriodoFacturacionId");

            migrationBuilder.CreateIndex(
                name: "IX_RutasUsuarioPeriodo_RutaId",
                schema: "ServiciosPublicos",
                table: "RutasUsuarioPeriodo",
                column: "RutaId");

            migrationBuilder.CreateIndex(
                name: "IX_Suscriptores_EstratoId",
                schema: "ServiciosPublicos",
                table: "Suscriptores",
                column: "EstratoId");

            migrationBuilder.CreateIndex(
                name: "IX_Suscriptores_MedidorId",
                schema: "ServiciosPublicos",
                table: "Suscriptores",
                column: "MedidorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Suscriptores_UsoId",
                schema: "ServiciosPublicos",
                table: "Suscriptores",
                column: "UsoId");

            migrationBuilder.CreateIndex(
                name: "IX_Suscriptores_ZonaId",
                schema: "ServiciosPublicos",
                table: "Suscriptores",
                column: "ZonaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Consumos",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "DescuentosAcueducto",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "DireccionesRuta",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "RutasUsuarioPeriodo",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "TiposVia",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetRoles",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "AspNetUsers",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Suscriptores",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Rutas",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Estratos",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Medidores",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Usos",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "Zonas",
                schema: "ServiciosPublicos");

            migrationBuilder.DropTable(
                name: "EstadosMedidor",
                schema: "ServiciosPublicos");
        }
    }
}
