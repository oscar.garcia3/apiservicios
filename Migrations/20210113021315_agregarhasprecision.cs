﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioPublicosAPI.Migrations
{
    public partial class agregarhasprecision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Porcentaje",
                schema: "ServiciosPublicos",
                table: "DescuentosAcueducto",
                type: "decimal(5,2)",
                precision: 5,
                scale: 2,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ValorMedicion",
                schema: "ServiciosPublicos",
                table: "Consumos",
                type: "decimal(30,10)",
                precision: 30,
                scale: 10,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Porcentaje",
                schema: "ServiciosPublicos",
                table: "DescuentosAcueducto",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldPrecision: 5,
                oldScale: 2);

            migrationBuilder.AlterColumn<decimal>(
                name: "ValorMedicion",
                schema: "ServiciosPublicos",
                table: "Consumos",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(30,10)",
                oldPrecision: 30,
                oldScale: 10);
        }
    }
}
