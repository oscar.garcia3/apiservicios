﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ServicioPublicosAPI.Migrations
{
    public partial class NombresUsuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                schema: "ServiciosPublicos",
                table: "AspNetUsers",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SurName",
                schema: "ServiciosPublicos",
                table: "AspNetUsers",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                schema: "ServiciosPublicos",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SurName",
                schema: "ServiciosPublicos",
                table: "AspNetUsers");
        }
    }
}
