﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.DTOs
{
    public class PaginacionDTO
    {
        public int Pagina { get; set; } = 1;
        private int _RegistrosPorPagina { get; set; } = 10;
        private readonly int cantidadMaxima = 50;
        public int RegistrosPorPagina
        {
            get
            {
                return _RegistrosPorPagina;
            }
            set
            {
                _RegistrosPorPagina = (value > cantidadMaxima) ? cantidadMaxima : value;
            }
        }

    }
}
