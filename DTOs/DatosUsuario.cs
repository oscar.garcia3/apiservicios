﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.DTOs
{
    public class DatosUsuario
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string SurName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
