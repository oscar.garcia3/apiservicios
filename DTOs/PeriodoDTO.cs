﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServicioPublicosAPI.DTOs
{
    public class PeriodoDTO
    {
      public int PeriodoID { get; set; }
      public int Anio { get; set; }
      public int Mes { get; set; }
      public DateTime FechaInicial { get; set; }
      public DateTime FechaFinal { get; set; }
      public DateTime FechaCorte { get; set; }
      public DateTime FechaLimite { get; set; }
      public bool Abierto { get; set; }
      public bool Activo { get; set; }
      public int UsuarioID { get; set; }
      public string Observacion { get; set; }
      public bool Cancelado { get; set; }
    }
}
